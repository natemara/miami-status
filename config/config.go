package config

import (
	"github.com/BurntSushi/toml"
	validator "gopkg.in/go-playground/validator.v9"
)

type Config struct {
	Mailgun MailgunConfig `validate:"required"`
	PingURL string        `validate:"required"`
	Term    string        `validate:"required"`
	User    []UserConfig  `validate:"required"`
}

type UserConfig struct {
	Email string   `validate:"required"`
	CRNs  []string `validate:"required"`
}

type MailgunConfig struct {
	Domain     string `validate:"required"`
	PrivateKey string `validate:"required"`
	PublicKey  string `validate:"required"`
	Sender     string `validate:"required"`
}

func Parse(filename string) (*Config, error) {
	config := new(Config)
	_, err := toml.DecodeFile(filename, config)
	if err != nil {
		return nil, err
	}

	validate := validator.New()
	err = validate.Struct(config)
	if err != nil {
		return nil, err
	}

	return config, nil
}
