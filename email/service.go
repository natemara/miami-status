package email

import (
	"fmt"

	mailgun "github.com/mailgun/mailgun-go"
	"gitlab.com/natemara/miami-status/config"
	"gitlab.com/natemara/miami-status/miami"
)

type EmailService struct {
	mailgun mailgun.Mailgun
	sender  string
}

func New(config config.MailgunConfig) EmailService {
	return EmailService{
		mailgun: mailgun.NewMailgun(
			config.Domain,
			config.PrivateKey,
			config.PublicKey,
		),
		sender: config.Sender,
	}
}

func (e *EmailService) SendEmails(freeSections []*miami.CourseFree) error {
	for _, course := range freeSections {
		message := e.mailgun.NewMessage(
			e.sender,
			"Your course is available!",
			fmt.Sprintf(
				"The course you requested to watch - CRN %s has a free spot! Act fast to get in!",
				course.CRN,
			),
			course.Emails...,
		)

		_, _, err := e.mailgun.Send(message)
		if err != nil {
			return err
		}
	}

	return nil
}
