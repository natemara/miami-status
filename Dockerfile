FROM golang:1.10.3-alpine as build
WORKDIR /go/src/gitlab.com/natemara/miami-status/
COPY main.go .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .

FROM centurylink/ca-certs
WORKDIR /app
EXPOSE 8000
COPY --from=build /go/src/gitlab.com/natemara/miami-status/app /app/app
ENTRYPOINT ["/app/app"]
